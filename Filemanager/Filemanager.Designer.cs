﻿namespace Filemanager
{
    partial class Filemanager
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader_fileName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader_date = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.textBox_preview = new System.Windows.Forms.TextBox();
            this.btn_pathAdd = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(12, 65);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(277, 518);
            this.treeView1.TabIndex = 0;
            this.treeView1.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.treeView1_BeforeExpand);
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader_fileName,
            this.columnHeader_date});
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(308, 65);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(480, 518);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.Click += new System.EventHandler(this.listView1_Click);
            // 
            // columnHeader_fileName
            // 
            this.columnHeader_fileName.Text = "Dateiname";
            this.columnHeader_fileName.Width = 200;
            // 
            // columnHeader_date
            // 
            this.columnHeader_date.Text = "Datum";
            this.columnHeader_date.Width = 100;
            // 
            // textBox_preview
            // 
            this.textBox_preview.Location = new System.Drawing.Point(807, 12);
            this.textBox_preview.Multiline = true;
            this.textBox_preview.Name = "textBox_preview";
            this.textBox_preview.Size = new System.Drawing.Size(539, 571);
            this.textBox_preview.TabIndex = 2;
            // 
            // btn_pathAdd
            // 
            this.btn_pathAdd.Font = new System.Drawing.Font("Arial Rounded MT Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_pathAdd.Location = new System.Drawing.Point(12, 12);
            this.btn_pathAdd.Name = "btn_pathAdd";
            this.btn_pathAdd.Size = new System.Drawing.Size(776, 47);
            this.btn_pathAdd.TabIndex = 3;
            this.btn_pathAdd.Text = "Verzeichnis hinzufügen";
            this.btn_pathAdd.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_pathAdd.UseVisualStyleBackColor = true;
            this.btn_pathAdd.Click += new System.EventHandler(this.btn_pathAdd_Click);
            // 
            // Filemanager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1363, 598);
            this.Controls.Add(this.btn_pathAdd);
            this.Controls.Add(this.textBox_preview);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.treeView1);
            this.Name = "Filemanager";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader_fileName;
        private System.Windows.Forms.ColumnHeader columnHeader_date;
        private System.Windows.Forms.TextBox textBox_preview;
        private System.Windows.Forms.Button btn_pathAdd;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}

