﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Filemanager
{
    public partial class Filemanager : Form
    {
        private string currentDir;
        private HashSet<string> addedDirectories = new HashSet<string>();
        private HashSet<string> traversedDirectories = new HashSet<string>();

        public Filemanager()
        {
            InitializeComponent();
            listView1.GridLines = true;
        }

        private void AddRootNode()
        {
            folderBrowserDialog1.ShowDialog();
            string path = folderBrowserDialog1.SelectedPath;

            TreeNode root = new TreeNode(path);
            treeView1.Nodes.Add(root);

            TraverseNode(root);
        }

        private void TraverseNode(TreeNode root)
        {
            if (!traversedDirectories.Contains(root.FullPath))
            {
                try
                {
                    DirectoryInfo directoryInfo = new DirectoryInfo(root.FullPath);

                    foreach (DirectoryInfo dir in directoryInfo.GetDirectories())
                    {
                        if (!addedDirectories.Contains(dir.FullName))
                        {
                            var currentNode = new TreeNode(dir.Name);
                            root.Nodes.Add(currentNode);
                            addedDirectories.Add(dir.FullName);
                        }
                    }

                    traversedDirectories.Add(root.FullPath);
                }
                catch (UnauthorizedAccessException)
                {
                }
            }
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            // Assign images to un-/ selected folders.
            treeView1.ImageList = new ImageList();
            treeView1.ImageList.Images.Add(Properties.Resources.folder_48);
            treeView1.ImageList.Images.Add(Properties.Resources.folder_open_48);
            treeView1.ImageIndex = 0;
            treeView1.SelectedImageIndex = 1;
        }


        private void btn_pathAdd_Click(object sender, EventArgs e)
        {
            AddRootNode();
        }

        private void treeView1_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            foreach (TreeNode node in e.Node.Nodes)
            {
                TraverseNode(node);
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            listView1.Items.Clear();

            //string[] files = System.IO.Directory.GetFiles(e.Node.FullPath, "*.txt");
            string[] files = System.IO.Directory.GetFiles(e.Node.FullPath);
            currentDir = e.Node.FullPath;

            foreach (string filePath in files)
            {
                string fileName = System.IO.Path.GetFileName(filePath);
                string dateString = Convert.ToString(System.IO.File.GetCreationTime(filePath));

                ListViewItem listViewItem = new ListViewItem(fileName);
                listViewItem.SubItems.Add(dateString);

                listView1.Items.Add(listViewItem);
            }
        }

        private void listView1_Click(object sender, EventArgs e)
        {
            string file = System.IO.Path.Combine(currentDir, listView1.SelectedItems[0].Text);
            textBox_preview.Text = System.IO.File.ReadAllText(file);
        }
    }
}